package APITests;

import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.*;


public class OtherPostsTest extends AbstractTest {

    @Test
    void OtherPostsWithSortOrderPage() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "1")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .spec(responseSpecification);
    }

    @Test
    void OtherPostsWithSortOrderALLPage() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ALL")
                .queryParam("page", "1")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .spec(responseSpecification);
    }

    @Test
    void otherPostsWithSortOrderPage() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "12")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .spec(responseSpecification);
    }

    @Test
    void OtherPostsWithSortOrderWithoutPage() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .statusCode(200);
    }

}
