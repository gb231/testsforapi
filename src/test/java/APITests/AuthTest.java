package APITests;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class AuthTest {

    @Test
    void getTokenForAuth () {
        Object response = given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", "Alna" )
                .formParam("password", "15e77a0473")
                .when()
                .post("https://test-stand.gb.ru/gateway/login")
                .then()
                .extract()
                .jsonPath()
                .get("token")
                .toString();
        System.out.println("Your Token for auth: " + response);
    }
    @Test
    void getTokenWithInvalidUserName(){
        given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username","Alina")
                .formParam("password", "15e77a0473")
                .when()
                .post("https://test-stand.gb.ru/gateway/login")
                .then()
                .statusCode(401);
    }
    @Test
    void getTokenWithInvalidPassword (){
        given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", "Alna")
                .formParam("password","1234")
                .when()
                .post("https://test-stand.gb.ru/gateway/login")
                .then()
                .statusCode(401);
    }



}
