package APITests;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.requestSpecification;

public abstract class AbstractTest {

    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String userName;
    private static String password;
    private static String baseUrl;
    private static String token;

    @BeforeAll

        static void initTest() throws IOException {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);

        userName =  prop.getProperty("userName");
        baseUrl= prop.getProperty("baseUrl");
        password =  prop.getProperty("password");
        token =  prop.getProperty("token");

        ResponseSpecification responseSpecification = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectStatusLine("HTTP/1.1 200 OK")
                .expectContentType(ContentType.JSON)
                .expectResponseTime(Matchers.lessThan(6000L))
                .build();

        RequestSpecification requestSpecification = new RequestSpecBuilder()
                .addHeader("X-Auth-Token", getToken())
               .setContentType(ContentType.JSON)
                .build();

        RestAssured.responseSpecification = responseSpecification;
        RestAssured.requestSpecification = requestSpecification;

    }

    public RequestSpecification getRequestSpecification(){
        return requestSpecification;
    }

    public static String getBaseUrl() {
        return baseUrl;
    }
    public static String getUserName() {
        return userName;
    }
    public static String getToken() {
        return token;
    }
    public static String getPassword() {
        return password;
    }
}
